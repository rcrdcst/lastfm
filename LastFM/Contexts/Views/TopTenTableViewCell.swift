//
//  TopTenTableViewCell.swift
//  LastFM
//
//  Created by ricardo silva on 07/08/2018.
//  Copyright © 2018 ricardo silva. All rights reserved.
//

import UIKit
import Kingfisher

class TopTenTableViewCell: UITableViewCell {

    @IBOutlet weak var albumName: UILabel!
    @IBOutlet weak var albumRank: UILabel!
    @IBOutlet weak var albumImage: ImageStyle!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setImage(url: String) {
        let urlString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        albumImage.kf.setImage(with: URL(string: urlString!), placeholder: UIImage(named: "music-placeholder"))
    }
}
