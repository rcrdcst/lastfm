//
//  ViewController.swift
//  LastFM
//
//  Created by ricardo silva on 07/08/2018.
//  Copyright © 2018 ricardo silva. All rights reserved.
//

import UIKit
import RxSwift
import RxDataSources
import StoreKit

class TopAlbunsVC: UIViewController {

    private let vm = LastFMVM()
    let bag = DisposeBag()
    let cellIdentifier = "TopTenTableViewCell"
    var numberAppRuns = 0
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = TOP_ALBUNS_TITLE
        
        numberAppRuns = UserDefaults.standard.integer(forKey: APP_RUNS)
        if numberAppRuns >= APP_MAX_RUNS {
            SKStoreReviewController.requestReview()
            UserDefaults.standard.set(1, forKey: APP_RUNS)
        } else {
            UserDefaults.standard.set(numberAppRuns + 1, forKey: APP_RUNS)
        }
        
        
        tableView.register(cellType: TopTenTableViewCell.self)
        
        vm.topAlbum().subscribe(onNext: { [unowned self] result in
            
            let items = Observable.just(result)
            
            items.bind(to: self.tableView.rx.items(cellIdentifier: self.cellIdentifier)) { (row, element, cell) in
                if let celltoUse = cell as? TopTenTableViewCell {
                    celltoUse.albumName.text = element.name
                    celltoUse.albumRank.text = element.attr.rank
                    guard let image = element.image.first?.text else {
                        return
                    }
                    celltoUse.setImage(url: image)
                }
                
            }
        }).disposed(by: bag)
        
        tableView.rx.modelSelected(Album.self).subscribe(onNext: { [weak self] item in
            
            guard let strongSelf = self else { return }
            
            guard let albumDetails = strongSelf.storyboard?.instantiateViewController(withIdentifier: StoryboardName.AlbumDetails) as? AlbumDetailsVC else {
                fatalError("SongDetailsVC not found")
            }
            
            if item.url.lowercased().range(of: FILTER_ALBUNS_URL) != nil {
                albumDetails.url = item.url
                if let navigator = strongSelf.navigationController {
                    navigator.pushViewController(albumDetails, animated: true)
                }
            } else {
                if let requestUrl = NSURL(string: item.url) {
                    UIApplication.shared.open(requestUrl as URL, options: [:], completionHandler: nil)
                }
            }
        }).disposed(by: bag)

    }
    
}
