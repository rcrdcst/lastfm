//
//  WebViewVC.swift
//  LastFM
//
//  Created by ricardo silva on 07/08/2018.
//  Copyright © 2018 ricardo silva. All rights reserved.
//

import UIKit
import WebKit

class AlbumDetailsVC: UIViewController {
    
    var url: String = ""
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.load(URLRequest(url: URL(string: url)!))
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
