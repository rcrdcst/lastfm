//
//  TopTag.swift
//  LastFM
//
//  Created by ricardo silva on 07/08/2018.
//  Copyright © 2018 ricardo silva. All rights reserved.
//

import Foundation

struct TopAlbum: Codable {
    let albums: Albums
}

struct Albums: Codable {
    let album: [Album]
    let attr: AlbumsAttr
    
    enum CodingKeys: String, CodingKey {
        case album
        case attr = "@attr"
    }
}

struct Album: Codable {
    let name, mbid, url: String
    let artist: Artist
    let image: [Image]
    let attr: AlbumAttr
    
    enum CodingKeys: String, CodingKey {
        case name, mbid, url, artist, image
        case attr = "@attr"
    }
}

struct Artist: Codable {
    let name, mbid, url: String
}

struct AlbumAttr: Codable {
    let rank: String
}

struct Image: Codable {
    let text: String
    let size: Size
    
    enum CodingKeys: String, CodingKey {
        case text = "#text"
        case size
    }
}

enum Size: String, Codable {
    case extralarge = "extralarge"
    case large = "large"
    case medium = "medium"
    case small = "small"
}

struct AlbumsAttr: Codable {
    let tag, page, perPage, totalPages: String
    let total: String
}
