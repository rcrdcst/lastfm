//
//  LastFMVM.swift
//  LastFM
//
//  Created by ricardo silva on 08/08/2018.
//  Copyright © 2018 ricardo silva. All rights reserved.
//

import Foundation
import RxSwift

final class LastFMVM {
    
    private var handler: LastFMService
    
    
    init() {
        self.handler = LastFMService()
    }
    
    func topAlbum() -> Observable<[Album]> {
        return handler.tagTopAlbum().map { [unowned self] response  -> [Album] in
            return response.albums.album
        }
        
    }
    
    
    
    
    
}
