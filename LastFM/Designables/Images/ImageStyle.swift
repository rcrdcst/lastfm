//
//  ImageStyle.swift
//  LastFM
//
//  Created by ricardo silva on 08/08/2018.
//  Copyright © 2018 ricardo silva. All rights reserved.
//

import UIKit

@IBDesignable public class ImageStyle: UIImageView{
    
    @IBInspectable var roundCorner: Bool = false {
        didSet {
            layer.cornerRadius = roundCorner ? self.frame.size.height/2 : self.frame.size.height
        }
    }
}
