//
//  StoryboardType.swift
//  LastFM
//
//  Created by ricardo silva on 08/08/2018.
//  Copyright © 2018 ricardo silva. All rights reserved.
//

import Foundation

struct StoryboardName {
    static let Home = "TopAlbunsVC"
    static let AlbumDetails = "AlbumDetailsVC"
}
