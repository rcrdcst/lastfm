//
//  LastFMService.swift
//  LastFM
//
//  Created by ricardo silva on 07/08/2018.
//  Copyright © 2018 ricardo silva. All rights reserved.
//

import Foundation
import RxSwift
import Moya
import RxMoya

struct LastFMService: LastFMServiceType {
    
    private var unsplash: MoyaProvider<LastFMAPI>
    
    
    init(unsplash: MoyaProvider<LastFMAPI> = MoyaProvider<LastFMAPI>(plugins: [NetworkLoggerPlugin(verbose: true)])) {
        self.unsplash = unsplash
    }
    
    func tagTopAlbum() -> Observable<TopAlbum> {
        return unsplash.rx
            .request(.tagTopAlbum())
            .filterSuccessfulStatusCodes()
            .map(TopAlbum.self)
            .asObservable()
    }
    
}
