//
//  LastFmServiceType.swift
//  LastFM
//
//  Created by ricardo silva on 07/08/2018.
//  Copyright © 2018 ricardo silva. All rights reserved.
//

import Foundation
import RxSwift

protocol LastFMServiceType {
    func tagTopAlbum() -> Observable<TopAlbum>
}

