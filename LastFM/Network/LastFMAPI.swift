//
//  LastFMAPI.swift
//  LastFM
//
//  Created by ricardo silva on 07/08/2018.
//  Copyright © 2018 ricardo silva. All rights reserved.
//

import Foundation
import Moya

enum LastFMAPI {
    case tagTopAlbum()
}

extension LastFMAPI: TargetType  {
    
    var baseURL: URL {
        guard let url = URL(string: "http://ws.audioscrobbler.com") else {
            fatalError("FAILED: http://ws.audioscrobbler.com")
        }
        return url
    }
    
    var path: String {
        switch self {
        case .tagTopAlbum:
            return "2.0/"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .tagTopAlbum:
            return .get
        }
        
    }
    
    
    var parameterEncoding: ParameterEncoding {
        return URLEncoding.default
    }
    
    var task: Task {
        switch self {
        case .tagTopAlbum:
            var params: [String: Any] = [:]
            params["method"] = "tag.gettopalbums"
            params["tag"] = "hiphop"
            params["limit"] = 10
            params["api_key"] = API_KEY
            params["format"] = "json"
            
            return .requestParameters(
                parameters: params,
                encoding: URLEncoding.default)
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var headers: [String : String]? {
        return nil
    }
    
}
